﻿using System;
using System.Diagnostics;

namespace Math2
{
    /// <summary>
    /// rational number
    /// </summary>
    public struct rat : IEquatable<rat>, IComparable<rat>
    {
        private readonly long m_numerator; // sign is used
        private readonly long m_denominator; // sign is unused


        #region constructors
        public rat(long numerator, long denominator)
        {
            if (0 == denominator) throw new DivideByZeroException("Denominator must not be zero.");
            var gcd = Algorithms.GreatestCommonDivisor(numerator, denominator);
            if (denominator > 0)
            {
                m_numerator = numerator / gcd;
                m_denominator = denominator / gcd;
            }
            else
            {
                m_numerator = -(numerator / gcd);
                m_denominator = -(denominator / gcd);
            }
        }

        public rat(long integer)
        {
            m_numerator = integer;
            m_denominator = 1;
        }

        public rat(byte naturalNumber) : this((long)naturalNumber) { }
        public rat(ushort naturalNumber) : this((long)naturalNumber) { }
        public rat(uint naturalNumber) : this((long)naturalNumber) { }

        public rat(sbyte integer) : this((long)integer) { }
        public rat(short integer) : this((long)integer) { }
        public rat(int integer) : this((long)integer) { }
        #endregion


        #region import
        public static implicit operator rat(sbyte integer) { return new rat(integer); }
        public static implicit operator rat(byte naturalNumber) { return new rat(naturalNumber); }
        public static implicit operator rat(short integer) { return new rat(integer); }
        public static implicit operator rat(ushort naturalNumber) { return new rat(naturalNumber); }
        public static implicit operator rat(int integer) { return new rat(integer); }
        public static implicit operator rat(uint naturalNumber) { return new rat(naturalNumber); }
        public static implicit operator rat(long integer) { return new rat(integer); }
        #endregion


        #region export
        public static explicit operator float(rat rationalNumber)
        {
            return (float)rationalNumber.m_numerator / (float)rationalNumber.m_denominator;
        }
        public static explicit operator double(rat rationalNumber)
        {
            return (double)rationalNumber.m_numerator / (double)rationalNumber.m_denominator;
        }
        public static explicit operator decimal(rat rationalNumber)
        {
            return (decimal)rationalNumber.m_numerator / (decimal)rationalNumber.m_denominator;
        }
        public override string ToString()
        {
            return (1 == m_denominator) ? m_numerator.ToString() : string.Format("{0}/{1}", m_numerator, m_denominator);
        }
        #endregion


        #region basic arithmetic operations
        public static rat operator *(rat lhs, rat rhs)
        {
            return new rat(lhs.m_numerator * rhs.m_numerator, lhs.m_denominator * rhs.m_denominator);
        }

        public static rat operator /(rat lhs, rat rhs)
        {
            return new rat(lhs.m_numerator * rhs.m_denominator, lhs.m_denominator * rhs.m_numerator);
        }

        public static rat operator +(rat lhs, rat rhs)
        {
            return new rat((lhs.m_numerator * rhs.m_denominator) + (rhs.m_numerator * lhs.m_denominator), lhs.m_denominator * rhs.m_denominator);
        }

        public static rat operator -(rat lhs, rat rhs)
        {
            return new rat((lhs.m_numerator * rhs.m_denominator) - (rhs.m_numerator * lhs.m_denominator), lhs.m_denominator * rhs.m_denominator);
        }
        #endregion


        #region comparison
        public static bool operator ==(rat lhs, rat rhs)
        {
            return (lhs.m_numerator == rhs.m_numerator) && (lhs.m_denominator == rhs.m_denominator);
        }

        public static bool operator !=(rat lhs, rat rhs)
        {
            return (lhs.m_numerator != rhs.m_numerator) || (lhs.m_denominator != rhs.m_denominator);
        }

        public override bool Equals(object obj)
        {
            return obj is rat && this == (rat)obj;
        }

        public override int GetHashCode()
        {
            return m_numerator.GetHashCode() ^ m_denominator.GetHashCode();
        }

        public bool Equals(rat other)
        {
            return this == other;
        }
        
        public int CompareTo(rat other)
        {
            var delta = this - other;
            return (0 == delta.m_numerator) ? 0 :
                ((delta.m_numerator > 0) ? +1 : -1);
        }
        #endregion
    }
}
