﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Math2
{
    public static class Algorithms
    {
        /// <summary>
        /// Find greatest common divisor of two numbers using Euclid's algorithm.
        /// </summary>
        /// <returns>natural number</returns>
        public static long GreatestCommonDivisor(long m, long n)
        {
            return (0 == n) ? ((m < 0) ? -m : m) : GreatestCommonDivisor(n, m % n);
        }
    }
}
