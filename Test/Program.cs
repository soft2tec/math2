﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Math2;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            rat r = 42;
            Console.WriteLine(r);

            rat x = new rat(2, -4);
            Console.WriteLine(x);
            rat y = new rat(-2, 4);
            Console.WriteLine(x);

            var rats = new rat[] { 1, 3, 2, 60, 4, 888, 1, 20 };
            Console.WriteLine("asorted:");
            for (int i = 0; i < rats.Length; ++i)
            {
                rats[i] = rats[i] / (i + 2);
                Console.WriteLine(rats[i]);
            }

            Array.Sort(rats);

            Console.WriteLine("sorted:");
            for (int i = 0; i < rats.Length; ++i)
            {
                Console.WriteLine(rats[i]);
            }
        }
    }
}
